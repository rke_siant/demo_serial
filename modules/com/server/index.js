console.log('server com run');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

const path = "COM1";

const port = new SerialPort(path, { baudRate: 256000 });
const parser = new Readline();
port.pipe(parser);

parser.on('data', (line) => {
    console.log(`> ${line}`);
    port.write('ROBOT RESPONSE WELL\n');
});

console.log('server com standby');
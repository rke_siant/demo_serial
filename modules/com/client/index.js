console.log('client com run');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

const path = "COM2";

const port = new SerialPort(path, { baudRate: 256000 });
const parser = new Readline();
port.pipe(parser);

parser.on('data', line => console.log(`> ${line}`));

port.write('ROBOT POWER ON\n');

console.log('client com standby');
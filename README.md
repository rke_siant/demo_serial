# Quick demo

For Windows

1. download&install **Virtual Port Driver 9.0 by Eltima Software** [this link](https://www.eltima.com/ru/products/vspdxp/)
2. create classic COM port pair. COM1-COM2
3. ``npm i``
4. ``npm run test_server``
5. ``npm run test_client``
6. observe result
